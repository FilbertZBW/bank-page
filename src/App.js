import React from 'react';
import Login from './Pages/Login/Login';
import bg from './assets/img.png'
// import './index.css';
// eslint-disable-next-line
import Home from './Pages/Home/Home'
import Tabs from "./Pages/Login/Tab";
import IntroPage from "./Pages/IntroPage/IntroPage";

class App extends React.Component{
  render(){
    return(
      <div style={{backgroundImage: `url(${bg})`, padding: 50}}>
          <Tabs/>
          <div className="flexbox-container">
              <Login/>
              <IntroPage/>
          </div>
      </div>
    )
  }
}

export default App;
