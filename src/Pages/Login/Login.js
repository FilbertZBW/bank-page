import React from 'react';
import Logo from '../../assets/credit-card.png'
import './login.css';

class Login extends React.Component{
    state={
        email:'',
        pwd:''
    }

    render(){
        return(
            <>
                <div className='div-login'>
                    <br/><br/>
                    <div className='div-login-logo'>
                        <img src={Logo} alt="Logo"/>
                    </div>
                    <div className='txt'>No hidden fees.</div>
                    <div className='txt'>No credit check.</div>
                    <div className='txt'>Instant pre-approval.</div>
                    <br/><br/><br/><br/><br/>
                    <div>
                        <form>
                            <input type='email' name='email' placeholder='email...' required/>
                            <input type='password' name='pwd' placeholder='password...' required/>
                            <button>Log In</button>
                        </form>
                    </div>
                </div>
            </>

        )
    }
}

export default Login;
