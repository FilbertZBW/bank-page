import React from "react";
const Tabs = () => {
    return (
        <div className="Tabs">
            {/* Tab nav */}
            <ul className="nav">
                <li className="active">Home</li>
                <li>About</li>
                <li>Q & A</li>
                <li>Contact us</li>
            </ul>
            <div className="outlet">
                {/* content will be shown here */}
            </div>
        </div>
    );
};
export default Tabs;
